# -*- coding: utf-8 -*-

import sys
import socket
import string
import time
import random
from datetime import date
#my modules
from parse import *
from message import *
from commandutils import *
from kevv import *

HOST=""
PORT=
IDENT=""
REALNAME=""
CHANLIST = [""]
readbuffer=""
NICKLIST = ["",""]

# Handles server issues
def sReplies(bufferedlinelist):
    if bufferedlinelist[0] == "ping":
        s.send("PONG %s\r\n" % bufferedlinelist[1])

    # if nick is in use pop another from nicklist stack
    if bufferedlinelist[1] == "433":
        s.send("NICK %s\r\n" % NICKLIST.pop())
        for CHANNEL in CHANLIST:   
            s.send("JOIN %s\n" % CHANNEL)

    # No nick was given
    if bufferedlinelist[1] == "431":
        print "Enter NICKLIST"
        exit()

    # erroneus nick
    if bufferedlinelist[1] == "432":
        print "You can't use this nick"
        exit()

    #rejoin when banned
    if bufferedlinelist[1] == "474":
        time.sleep(500)
        s.send("JOIN %s\n" % bufferedlinelist[3])

    #rejoin when kicked
    if bufferedlinelist[1] == "kick":
        time.sleep(10)
        s.send("JOIN %s\n" % bufferedlinelist[2])

# logging for every channel the bot is in
def logger(bufferedlinelist):
    if bufferedlinelist[1] == "privmsg":
        filename = ("%s.log" % bufferedlinelist[2])
        f = open(filename, 'a+')
        if bufferedlinelist[2] in CHANLIST:
            f.write("<%s>:%s\n" % (getNickFromLine(bufferedlinelist), getMessageFromLine(bufferedlinelist)))
            f.close()

def commands(sender,bufferedlinelist):
    if len(bufferedlinelist) >= 4:
        if bufferedlinelist[3] == ":@dtc":
            today = date.today()
            christmas = date(today.year, 12, 25)

            if christmas < today:
                christmas = christmas.replace(year=today.year + 1)

            tUntilChristmas = abs(christmas - today)

            s.send("PRIVMSG %s :%i days until Christmas!\r\n" % (sender, tUntilChristmas.days))

        if bufferedlinelist[3] == ":@trole":
            s.send("PRIVMSG %s :( ͡° ͜ʖ ͡°)\r\n" % (sender))

        if bufferedlinelist[3] == ':@roll' or bufferedlinelist[3] == ':.roll':
            s.send("PRIVMSG %s :[\x033rolled\x03] %i\r\n" % (sender, random.randint(0,100)))


        if bufferedlinelist[3] == ":@kevv":
            message = getMessageFromLine(bufferedlinelist)
            newmessage = message.replace("@kevv ","")
            filename = ("%s.log" % bufferedlinelist[2])
            if newmessage == "" or newmessage == " ":
                msg = choseRandomLine(filename)
                s.send("PRIVMSG %s :%s \r\n" % (sender, msg))
            else:
                msg = choseRandomLine(filename, newmessage.strip())
                s.send("PRIVMSG %s :%s \r\n" % (sender, msg))


        elif len(bufferedlinelist) >= 5:      
            if bufferedlinelist[3] == ":@ud":
                message = getMessageFromLine(bufferedlinelist)
                newmessage = message.replace("@ud ","")
                udefined = define(newmessage)

                try:
                    contents = udefined[0]
                    s.send("PRIVMSG %s :[\x033UD\x03]: %s \r\n" % (sender, contents['def'].replace('\n','')))

                    try:
                        s.send("PRIVMSG %s :[\x033Example\x03]: %s \r\n" % (sender, contents['example'].replace('\n','')))
                    except:
                        pass

                except:
                    s.send("PRIVMSG %s :[\x033UD\x03]: No info found :3\r\n" % sender)
                    pass

            elif bufferedlinelist[3] == ":@next":
                message = getMessageFromLine(bufferedlinelist)
                newmessage = message.replace('@next', "")
                
                try:
                    tvinfo = showInfo(newmessage)
                    s.send("PRIVMSG %s :[%s] :: [Next: %s] [Airs: %s]\r\n" % (sender, tvinfo[1][1], tvinfo[7][1].replace('^', ' '), tvinfo[15][1]))

                except:
                    s.send("PRIVMSG %s :No info found :3\r\n" % sender)
                    pass

            elif bufferedlinelist[3] == ":@define":
                message = getMessageFromLine(bufferedlinelist)
                newmessage = message.replace('@define', "")

                try:
                    dcontents = dictdefine(newmessage)
                    s.send("PRIVMSG %s :[\x033Define\x03]: %s\r\n" % (sender, dcontents[0].text))

                except:
                    s.send("PRIVMSG %s :No definition :3\r\n" % sender)


            elif bufferedlinelist[3] == ":@weather":
                message = getMessageFromLine(bufferedlinelist)
                newmessage = message.replace('@weather', "")
                print "this is the new message: " + newmessage

                try:
                    weatherData = weather(str(newmessage.strip()))['current_observation']
                    city = weatherData['display_location']['full']
                    cWeather = weatherData['weather']
                    temperature = weatherData['temperature_string']
                    rHumidity = weatherData['relative_humidity']
                    print city + " " + cWeather + " " + temperature + " " + rHumidity
                    s.send("PRIVMSG %s :%s [weather: %s][temperature: %s][relative humidity: %s]\r\n" % (sender, city, cWeather, temperature, rHumidity))

                except:
                    s.send("PRIVMSG %s :No info found :3\r\n" % sender)

            elif bufferedlinelist[3] == ":@google":
                message = getMessageFromLine(bufferedlinelist)
                newmessage = message.replace('@google', "")
                
                try:
                    searchurl = lookup(newmessage)[0]['url']
                    s.send("PRIVMSG %s :[\x033Google\x03]: Top google result for \"\x033%s\x03\": %s \r\n" % (sender, newmessage, searchurl))
                except:
                    s.send("PRIVMSG %s :No info found :3\r\n" % sender)
                    pass

s=socket.socket()
s.connect((HOST,PORT))
s.send("NICK %s\r\n" % NICKLIST[0])
s.send("USER %s %s bla :%s\r\n" % (IDENT,HOST,REALNAME))
for CHANNEL in CHANLIST:
    s.send("JOIN %s\n" % CHANNEL)

while 1:
    readbuffer = readbuffer + s.recv(1024)
    temp = string.split(readbuffer, "\n")
    readbuffer = temp.pop()

    for line in temp:
        line = string.split(string.rstrip(line.lower()))

        print line
        sReplies(line)

        if line[1] == "privmsg":

            if line[2] in CHANLIST:
                commands(getChannelFromLine(line), line)

            else:
                commands(getNickFromLine(line), line)

        logger(line)