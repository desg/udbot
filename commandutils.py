#!/usr/bin/env python
#
# Simple interface to urbandictionary.com
#
# Author: Roman Bogorodskiy <bogorodskiy@gmail.com>

import sys

if sys.version < '3':
    from urllib import quote as urlquote
    from urllib2 import urlopen
    from HTMLParser import HTMLParser
else:
    from urllib.request import urlopen
    from urllib.parse import quote as urlquote
    from html.parser import HTMLParser


class TermType(object):
    pass


class TermTypeRandom(TermType):
    pass


class UrbanDictParser(HTMLParser):

    def __init__(self, *args, **kwargs):
        HTMLParser.__init__(self, *args, **kwargs)
        self._section = None
        self.translations = []

    def handle_starttag(self, tag, attrs):
        attrs_dict = dict(attrs)

        if tag != "div":
            return

        div_class = attrs_dict.get('class')
        if div_class in ('word', 'meaning', 'example'):
            self._section = div_class
            if div_class == 'word':  # NOTE: assume 'word' is the first section
                self.translations.append(
                    {'word': '', 'def': '', 'example': ''})

    def handle_endtag(self, tag):
        if tag == 'div':
            #NOTE: assume there is no nested <div> in the known sections
            self._section = None

    def handle_data(self, data):
        if not self._section:
            return

        if self._section == 'meaning':
            self._section = 'def'
        elif self._section == 'word':
            data = data.strip()

        self.translations[-1][self._section] += normalize_newlines(data)


def normalize_newlines(text):
    return text.replace('\r\n', '\n').replace('\r', '\n')


def define(term):
    if isinstance(term, TermTypeRandom):
        url = "http://www.urbandictionary.com/random.php"
    else:
        url = "http://www.urbandictionary.com/define.php?term=%s" % \
              urlquote(term)

    f = urlopen(url)
    data = f.read().decode('utf-8')

    urbanDictParser = UrbanDictParser()
    urbanDictParser.feed(data)

    return urbanDictParser.translations


# Wordnik Wrapper
# Takes a word and outputs a definition if there is one

from wordnik import *

def dictdefine(word):
    replacedword = word.strip()
    apiUrl = 'http://api.wordnik.com/v4'
    apiKey = '98f47f77bcf4336a25002080d1e075ac9dd8751b07bc2eb54'
    client = swagger.ApiClient(apiKey, apiUrl)

    wordApi = WordApi.WordApi(client)
    dictdefined = wordApi.getDefinitions(replacedword)

    return dictdefined


# TV Rage Wrapper
# Takes a tv show and displays tons of info about it

import requests

def showInfo(tvname):
    tvname = tvname.replace(' ', '+')
    tvinfo = requests.get("http://services.tvrage.com/tools/quickinfo.php?show=" + tvname).text
    try:
        tvdata = tvinfo.split('\n')
        tvinfo = []
        for data in tvdata:
            data = data.split('@')
            tvinfo.append(data)

        return tvinfo
    except:
        return None

# Weather Underground

def weather(area):
    apikey = "897522adf011af15"
    areaQuery = requests.get("http://autocomplete.wunderground.com/aq?query=%s" % area).json()

    searchData = areaQuery['RESULTS'][0]['l'] #Returns the first result

    jsonData = requests.get("http://api.wunderground.com/api/%s/conditions%s.json" % (apikey , searchData)).json()

    return jsonData


def rmUnwanted(string):
    string = string.replace("\n", " ")
    string = string.replace("<b>", "")
    string = string.replace("</b", "")

    return string

def lookup(search):
    search = search.replace(" ", "+")

    url = "https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q="
    url += search
    headers = {'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0 Iceweasel/30.0'}

    rJson = requests.get(url, headers=headers).json()['responseData']['results']

    searchResults = []

    for result in rJson:
        searchResults.append({'url' : result['url'],
                                'title' : rmUnwanted(result['title'])})

    return searchResults