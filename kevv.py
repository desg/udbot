import random

#returns a list of every line in log
def getListLog(filename):
	f = open(filename, 'r')

	ListLog = []
	for line in f:
		ListLog.append(line)

	return ListLog

# chooses a random name from from the nicks in filename
def choseRandomName(filename):
	listLog = getListLog(filename)
	namelist = []
	for name in listLog:
		newname = name.split(':')[0].replace('<', '').replace('>', '')
		if newname not in namelist:
			namelist.append(newname)
	return random.choice(namelist)

# chooses a random line from getListLog() with respect to name
def choseRandomLine(filename, name=None):
	if not name:
		name = choseRandomName(filename)
	
	listLog = getListLog(filename)
	templist = []
	for line in listLog:
		if line.split(':')[0] == '<'+name+'>':
			templist.append(line)

	if len(templist) >= 1:
		randomLine = random.choice(templist)
		# this can be bad fix when I come up with a better solution
		return randomLine.replace(":", " ")
	else:
		return None